// Import images
// import ComingSoon from '../images/Coming-Soon.jpg'
import MeetUpScreenshot from '../images/Meetup.jpg'
import HookedScreenshot from '../images/Hooked-Screenshot.png'
import ConferenceGoScreenshot from '../images/Conference-Go-Screenshot.png'
import CarCarScreenshot from '../images/Car-Car-Screenshot.png'
import { FaReact, FaBootstrap } from 'react-icons/fa';
import { BiLogoMongodb, BiLogoPostgresql, BiLogoDjango, BiLogoJavascript, BiLogoPython } from 'react-icons/bi';
import { SiTailwindcss, SiRabbitmq } from 'react-icons/si';

export const projectsData = [
	{
		id: 1,
		title: 'Meetup',
		category: 'Web Application',
		img: MeetUpScreenshot,
		description: `Meetup provides the convenience of an effortless scheduling app designed to facilitate the planning of gatherings with friends. Hosts are able to create a "Meetup" by sending invites to other friends. The application utilizes MongoDB and FastAPI for its backend and Javascript/JSX for its frontend with React.`,
		gitlabhref: 'https://gitlab.com/smjr/meet-up',
		icons: [FaReact, BiLogoMongodb, SiTailwindcss, BiLogoPython, BiLogoJavascript]
	},
	{
		id: 2,
		title: 'Hooked',
		category: 'Web Application',
		img: HookedScreenshot,
		description: "This is a fast and efficient fishing location finder app that leverages FastAPI and SQL, featuring a forum and a visually appealing interface styled with Tailwind. Each fishing location is populated with its own database of fish found in the area. The backend is powered by Python and PostgreSQL, while JavaScript adds dynamic interactivity.",
		gitlabhref: 'https://gitlab.com/hooked2/module3-project-gamma',
		deploy: 'https://hooked2.gitlab.io/module3-project-gamma/',
		icons: [FaReact, BiLogoPostgresql, SiTailwindcss, BiLogoPython, BiLogoJavascript]
	},
	{
		id: 3,
		title: 'Conference Go',
		category: 'Web Application',
		img: ConferenceGoScreenshot,
		description: "Designed with Python and Django, this application serves as a comprehensive conference management tool for both attendees and hosts. Leveraging the power of queues, pub/sub, and polling techniques, it ensures efficient handling of conference-related information, enabling seamless communication and coordination between all participants.",
		gitlabhref: 'https://gitlab.com/skoreancho/fearless-frontend',
		icons: [FaReact, BiLogoDjango, FaBootstrap, SiRabbitmq, BiLogoPython, BiLogoJavascript]
	},
	{
		id: 4,
		title: 'CarCar',
		category: 'Web Application',
		img: CarCarScreenshot,
		description: "CarCar is an application for managing car sales and services built with Python and Django. It implements effective information handling techniques such as pub/sub and polling. To enhance data flow, the application takes advantage of Insomnia, a tool that optimizes the overall functionality",
		gitlabhref: 'https://gitlab.com/skoreancho/car-car-project-beta',
		icons: [FaReact, BiLogoDjango, FaBootstrap, BiLogoPython, BiLogoJavascript]
	}
];
