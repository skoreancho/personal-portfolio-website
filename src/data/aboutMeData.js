export const aboutMeData = [
	{
		id: 1,
		bio: 'I bring a strong foundation in full-stack web development to the table. With expertise in JavaScript, Python, React, and Node.js, I have honed my skills through immersive projects and hands-on learning experiences. I am driven by the opportunity to create scalable and efficient web applications while solving complex problems.',
	},
	{
		id: 2,
		bio: 'I thrive in team environments where I can contribute my skills and learn from others. With a commitment to continuous growth and a love for tackling challenges head-on, I am excited to embark on new opportunities where I can make a meaningful impact. If you are seeking a results-oriented software engineer, let\'s connect and explore how I can contribute to your projects.',
	},
];
