// Import images
import MeetUpScreenshot from '../images/Meetup.jpg'
import HookedScreenshot from '../images/Hooked-Screenshot.png'
import ConferenceGoScreenshot from '../images/Conference-Go-Screenshot.png'
import CarCarScreenshot from '../images/Car-Car-Screenshot.png'

import {
	FiFacebook,
	FiInstagram,
	FiLinkedin,
	FiTwitter,
	FiYoutube,
} from 'react-icons/fi';

export const singleProjectData = {
	ProjectHeader: {
		title: 'Project Details'
	},
	ProjectImages: [
		{
			id: 1,
			title: 'Meetup',
			img: MeetUpScreenshot,
		},
		{
			id: 2,
			title: 'Hooked',
			img: HookedScreenshot,
		},
		{
			id: 3,
			title: 'Conference GO',
			img: ConferenceGoScreenshot,
		},
		{
			id: 4,
			title: 'CarCar',
			img: CarCarScreenshot,
		}
	],
	ProjectInfo: {
		ClientHeading: 'About Client',
		CompanyInfo: [
			{
				id: 1,
				title: 'Name',
				details: 'Company Ltd',
			},
			{
				id: 2,
				title: 'Services',
				details: 'UI Design & Frontend Development',
			},
			{
				id: 3,
				title: 'Website',
				details: 'https://company.com',
			},
			{
				id: 4,
				title: 'Phone',
				details: '555 8888 888',
			},
		],
		ObjectivesHeading: 'Objective',
		ObjectivesDetails:
			'Lorem ipsum dolor sit amet consectetur adipisicing elit. Optio, natus! Quibusdam enim quod in esse, mollitia molestias incidunt quas ipsa accusamus veniam.',
		Technologies: [
			{
				title: 'Tools & Technologies',
				techs: [
					'HTML',
					'CSS',
					'JavaScript',
					'Vue.js',
					'TailwindCSS',
					'AdobeXD',
				],
			},
		],
		ProjectDetailsHeading: 'Challenge',
		ProjectDetails: [
			{
				id: 1,
				details:
					'Meetup is an application',
			},
			{
				id: 2,
				details:
					'Hooked is an application geared for fishing enthusiasts and beginners alike. With its Python and PostgreSQL backend, the app ensures optimal performance and reliability. At its core, the app provides a system for checking out fishing locations by viewing the types of fish available. With verified information based on community inputs, it offers accurate predictions to help users discover productive fishing spots effortlessly. Beyond location tracking, the app includes a vibrant forum where anglers can connect, share knowledge, and engage in discussion. To create a visually appealing and responsive interface, the app utilizes the Tailwind styling framework to be viewable on mobile and desktop. The app relies on a stack consisting of FastAPI for its backend, SQL for efficient data management, and JavaScript for interactivity and real-time updates. In summary, this application acts as a tool for fishers and a collaborative forum to foster a community of anglers in their pursuit of the perfect catch. Its tech stack, including FastAPI, SQL, Tailwind, Python, PostgreSQL, and JavaScript, enables a seamless and efficient fishing experience.',
			},
			{
				id: 3,
				details:
					'Conference GO is an application designed to maange conferences as either a host or an attendee. Built with Python and Django, Conference Go handles event schedules, speaker details, attendee registrations, and more with pub/sub, queues, and polling.',
			},
			{
				id: 4,
				details:
					'CarCar is a car sales/service management app built with Python and Django. By implementing Django forms, users are able to easily add car manufacturers, vehicle models, as well as purchase available automobiles. If a vehicle is purchased from the site, the user is marked as a VIP client in the backend and are provided benefits for any services from CarCar. The application uses both pub/sub and polling to ensure effecient data flow.',
			},
		],
		SocialSharingHeading: 'Share This',
		SocialSharing: [
			{
				id: 1,
				name: 'Twitter',
				icon: <FiTwitter />,
				url: 'https://twitter.com/realstoman',
			},
			{
				id: 2,
				name: 'Instagram',
				icon: <FiInstagram />,
				url: 'https://instagram.com/realstoman',
			},
			{
				id: 3,
				name: 'Facebook',
				icon: <FiFacebook />,
				url: 'https://facebook.com/',
			},
			{
				id: 4,
				name: 'LinkedIn',
				icon: <FiLinkedin />,
				url: 'https://linkedin.com/',
			},
			{
				id: 5,
				name: 'Youtube',
				icon: <FiYoutube />,
				url: 'https://www.youtube.com/c/StomanStudio',
			},
		],
	},
	RelatedProject: {
		title: 'Related Projects',
		Projects: [
			{
				id: 1,
				title: 'Meetup',
				img: MeetUpScreenshot,
			},
			{
				id: 2,
				title: 'Hooked',
				img: HookedScreenshot,
			},
			{
				id: 3,
				title: 'Conference Go',
				img: ConferenceGoScreenshot,
			},
			{
				id: 4,
				title: 'CarCar',
				img: CarCarScreenshot,
			},
		],
	},
};
