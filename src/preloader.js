import React from 'react';

export const Preloader = () => {
  return (
    <div className="preloader-container">
      <div className="loader"></div>
    </div>
  );
};
