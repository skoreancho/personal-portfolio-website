import React, { useState} from 'react';
import { CarouselItem } from "./CarouselItem";
import { projectsData } from "../data/projects";

export const Carousel = () => {
    const [activeIndex, setActiveIndex] = useState(0);

    const items = projectsData;

    const updateIndex = (newIndex) => {
        if (newIndex < 0) {
            newIndex = 0;
        } else if (newIndex >= items.length) {
            newIndex = items.length - 1
        }
        setActiveIndex(newIndex)
    }

    return (
        <div className="outer h-full items-center">
        <div className="carousel rounded-tr-lg rounded-tl-lg border-2 border-black-500">
            <div className="inner items-center" style ={{ transform: `translate(-${activeIndex * 100}%)`}}>
                {items.map((item) => {
                    return (
                        <>
                        <CarouselItem item={item}/>
                        </>
                    )
                })}
            </div>
            <div className="carousel-buttons bg-white">
                <button onClick={() => {
                    updateIndex(activeIndex - 1);
                }}
                className="button-arrow">
                    <span className="material-symbols-outlined">
                        arrow_back_ios_new
                    </span>
                </button>
                <div className="indicators">
                    {items.map((item, index)=> {
                        return (
                            <button onClick={() => {
                                updateIndex(index);
                            }} className='indicator-buttons'>
                                <span className={`material-symbols-outlined ${index===activeIndex? "indicator-symbol-active": "indicator-symbol"}`}>
                                    radio_button_checked
                                </span>
                            </button>
                        )
                    })}
                </div>
                <button onClick={() => {
                        updateIndex(activeIndex + 1);
                    }}
                    className="button-arrow">
                    <span className="material-symbols-outlined">
                        arrow_forward_ios
                    </span>
                </button>
            </div>
        </div>

        </div>
    )
}
