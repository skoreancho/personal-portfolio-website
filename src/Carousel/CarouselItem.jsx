import React from "react";
import { FaReact, FaBootstrap } from 'react-icons/fa';
import { BiLogoMongodb, BiLogoPostgresql, BiLogoDjango, BiLogoDocker, BiLogoJavascript, BiLogoPython } from 'react-icons/bi';
import { IoLogoCss3 } from 'react-icons/io';
import { AiFillHtml5, AiFillGithub } from 'react-icons/ai';
import { SiRedux, SiTailwindcss, SiFastapi, SiRabbitmq } from 'react-icons/si';

export const CarouselItem = ({ item }) => {

    const iconComponents = {
        FaReact,
        FaBootstrap,
        BiLogoMongodb,
        BiLogoPostgresql,
        BiLogoDjango,
        BiLogoDocker,
        BiLogoJavascript,
        BiLogoPython,
        IoLogoCss3,
        AiFillHtml5,
        AiFillGithub,
        SiRedux,
        SiTailwindcss,
        SiFastapi,
        SiRabbitmq,
      };

    return (
        <div className="carousel-item">
            <div>
                <img className="" src={item.img} alt="carousel"/>
                <div
                    className="carousel-item-text flex-row"
                >
                    <div className="flex">
                        {item.icons.map((Icon, index) => (
                        <Icon key={index} size={32} className="flex justify-right" />
                        ))}
                    </div>
                    <h3 className="h2">{item.title}</h3>
                    <div className='h3 description'>{item.description}</div>
                </div>
                <div className="flex flex-row justify-center gap-8">
                    {item.deploy && (
                        <a href={item.deploy} className="flex justify-center text-xl font-arial font-bold text-blue-500 hover:opacity-50" target="_blank" rel="noopener noreferrer">Deployed Site</a>
                    )}
                    <a href={item.gitlabhref} className="flex justify-center text-xl font-arial font-bold text-blue-500 hover:opacity-50" target="_blank" rel="noopener noreferrer">Gitlab Repo</a>
                </div>
            </div>
        </div>
    )
}
