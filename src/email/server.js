const sendEmail = require('./sendEmail');
const express = require('express');
const app = express();

app.use(express.json());

app.post('/contact', (req, res) => {
  const formData = req.body;
  sendEmail(formData);
  res.status(200).json({ message: 'Email sent successfully' });
});

const port = 3000;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
