import { AnimatePresence } from 'framer-motion';
import { lazy, Suspense, useState, useEffect } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import ScrollToTop from './components/ScrollToTop';
import AppFooter from './components/shared/AppFooter';
import AppHeader from './components/shared/AppHeader';
import './css/tailwind.css';
import './css/App.css';
import UseScrollToTop from './hooks/useScrollToTop';
import { Preloader } from './preloader';

const About = lazy(() => import('./pages/AboutMe'));
const Contact = lazy(() => import('./pages/Contact.jsx'));
const Home = lazy(() => import('./pages/Home'));
const Projects = lazy(() => import('./pages/Projects'));
const ProjectSingle = lazy(() => import('./pages/ProjectSingle.jsx'));


function App() {
	const [loading, setLoading] = useState(true);

	useEffect(() => {
		const timer = setTimeout(() => {
			setLoading(false);
		}, 2000);

		return () => clearTimeout(timer);
	}, []);

	return (
		<AnimatePresence>
			<div id="root" className="bg-secondary-light dark:bg-primary-dark transition duration-300">
				<Router>
					<ScrollToTop />
					<AppHeader />
					{loading ? (
						<Preloader />
					) : (
						<Suspense fallback={""}>
							<Routes>
								<Route path="/" element={<Home />} />
								<Route path="projects" element={<Projects />} />
								<Route
									path="projects/single-project"
									element={<ProjectSingle />}
								/>

								<Route path="about" element={<About />} />
								<Route path="contact" element={<Contact />} />
							</Routes>
						</Suspense>
					)}
					<AppFooter />
				</Router>
				<UseScrollToTop />
			</div>
		</AnimatePresence>
	);
}

export default App;
