import useThemeSwitcher from '../../hooks/useThemeSwitcher';
import { FiArrowDownCircle } from 'react-icons/fi';
import developerLight from '../../images/developer.svg';
import developerDark from '../../images/developer-dark.svg';
import { motion } from 'framer-motion';
import profileImage from '../../images/linkedin profile photo.jpg';
import Typer from '../../typingEffect';
import ResumeOverlay from './ResumeOverlay';
import { useState } from 'react'
import { FaSearch } from 'react-icons/fa'

const AppBanner = () => {
	const [activeTheme] = useThemeSwitcher();
	const [showOverlay, setShowOverlay] = useState(false);

	const handleViewResume = () => {
		setShowOverlay(true);
	};

	const handleCloseOverlay = () => {
		setShowOverlay(false);
	};

	return (
		<motion.section
			initial={{ opacity: 0 }}
			animate={{ opacity: 1 }}
			transition={{ ease: 'easeInOut', duration: 0.9, delay: 0.2 }}
			className="container mt-[0%] mb-[5%] 2xl:mt-[10%] lg:mb-[10%] flex flex-wrap mx-auto "
		>
			<div className="centering 2xl:mt-[10%] mt-[5%]">
				<motion.h1
					initial={{ opacity: 0 }}
					animate={{ opacity: 1 }}
					transition={{
						ease: 'easeInOut',
						duration: 0.9,
						delay: 0.1,
					}}
					className="font-general-semibold text-2xl text-4xl text-center text-ternary-dark dark:text-primary-light uppercase"
				>
					Sean Cho
				</motion.h1>
				<motion.p
					initial={{ opacity: 0 }}
					animate={{ opacity: 1 }}
					transition={{
						ease: 'easeInOut',
						duration: 0.9,
						delay: 0.2,
					}}
					className="font-general-medium mt-4 text-3xl text-center leading-normal text-[#576CBC] dark:text-[#A5D7E8]"
				>
					<Typer/>
				</motion.p>
				<motion.p
					initial={{ opacity: 0 }}
					animate={{ opacity: 1 }}
					transition={{
						ease: 'easeInOut',
						duration: 0.9,
						delay: 0.2,
					}}
					className="font-general-medium mt-4 text-xl text-center leading-normal text-gray-500 dark:text-gray-200"
				>
					I'm <motion.span className="text-green-400">Sean Cho</motion.span>, a passionate software developer, and I'm thrilled to welcome you to my software portfolio! I have a deep love for building innovative solutions that make a positive impact. With a background in Full Stack Development, I enjoy bringing ideas to life through elegant and efficient code.
				</motion.p>
				<motion.div
					initial={{ opacity: 0 }}
					animate={{ opacity: 1 }}
					transition={{
						ease: 'easeInOut',
						duration: 0.9,
						delay: 0.3,
					}}
					className="sm:block"
				>
					<button
						onClick={handleViewResume}
						className="mx-auto font-general-medium flex justify-center items-center w-36 sm:w-48 mt-12 mb-6 sm:mb-0 text-lg border border-indigo-200 dark:border-ternary-dark py-2.5 sm:py-3 shadow-lg rounded-lg bg-indigo-50 focus:ring-1 focus:ring-indigo-900 hover:bg-indigo-500 text-gray-500 hover:text-white duration-500"
						aria-label="View Resume"
						>
						<FaSearch className="mr-2 sm:mr-3 h-5 w-5 sn:w-6 sm:h-6 duration-100"></FaSearch>
						<span className="text-sm sm:text-lg font-general-medium duration-100">
							View CV
						</span>
						</button>
					</motion.div>
					{showOverlay && <ResumeOverlay onClose={handleCloseOverlay} />}
			</div>
			<motion.div
				initial={{ opacity: 0, y: -180 }}
				animate={{ opacity: 1, y: 0 }}
				transition={{ ease: 'easeInOut', duration: 0.9, delay: 0.2 }}
				className="text-right float-right mx-auto sm:mt-0"
			>
				<img
					src={profileImage}
					alt="Developer"
					className="rounded-full mt-5 mb-5 profile-img"
				/>
			</motion.div>
		</motion.section>
	);
};

export default AppBanner;
