import React from 'react';
import Resume from '../../images/Sean-Cho-Resume.jpg'

const ResumeOverlay = ({ onClose }) => {
  return (
    <div className="fixed top-0 left-0 w-full h-full bg-black bg-opacity-75 flex justify-center items-center z-50" onClick={onClose}>
      <img src={Resume} alt="Resume" className="max-h-full max-w-full" />
    </div>
  );
};

export default ResumeOverlay;
