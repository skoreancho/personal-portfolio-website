import { Carousel } from '../../Carousel/Carousel';
import { useEffect } from 'react';
import { motion, useAnimation } from 'framer-motion';
import { useInView } from 'react-intersection-observer';

const ProjectsGrid = () => {

	const controls = useAnimation();

	const [ref, inView] = useInView({
		triggerOnce: true,
		rootMargin: '-100px 0px', // Adjust rootMargin as needed
	});

	useEffect(() => {
		if (inView) {
		controls.start({
			opacity: 1,
			y: 0,
			transition: { ease: 'easeInOut', duration: 0.7, delay: 0.2 },
		});
		}
	}, [controls, inView]);

	return (
		<section className="h-full max-w-3/4 py-5 sm:py-10">
			<div className="pt-10 text-center">
				<p className="h1 text-center text-ternary-dark dark:text-ternary-light">
					Projects portfolio
				</p>
			</div>

			<div>
				<h3
					className="h2
                        text-center text-secondary-dark
                        dark:text-ternary-light
                        text-md
                        sm:text-xl
                        mb-2
                        "
				>
					Browse through project gallery
				</h3>
			</div>
			<motion.div
			ref={ref}
			initial={{ opacity: 0, y: 180 }}
			animate={ controls }
			transition={{
				ease: 'easeInOut',
				duration: 0.7,
				delay: 0.2,
			}}
			>
				<Carousel />
			</motion.div>
		</section>
	);
};

export default ProjectsGrid;
