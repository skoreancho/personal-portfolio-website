import React, { useState, useEffect } from 'react';
import emailjs from 'emailjs-com'
import { motion, useAnimation } from 'framer-motion';
import { useInView } from 'react-intersection-observer';

const ContactForm = () => {

	const [formData, setFormData] = useState({
		name: '',
		email: '',
		subject: '',
		message: '',
	});

	const controls = useAnimation();

	const [ref, inView] = useInView({
		triggerOnce: true,
		rootMargin: '-100px 0px', // Adjust rootMargin as needed
	});

	useEffect(() => {
		if (inView) {
		controls.start({
			opacity: 1,
			y: 0,
			transition: { ease: 'easeInOut', duration: 0.7, delay: 0.2 },
		});
		}
	}, [controls, inView]);

	const [isSuccess, setIsSuccess] = useState(false);

	const sendEmail = (e) => {
		e.preventDefault();

		const serviceId = 'service_zbtm7r2';
		const templateId = 'template_cew9jp9';
		const key = 'aRPRU3-dmvoINGpXp'

		emailjs.sendForm(serviceId, templateId, e.target, key)
			.then(() => {
			console.log('Email sent successfully');
			setIsSuccess(true);
			setFormData({
				name: '',
				email: '',
				subject: '',
				message: '',
			});
			// Perform any additional actions after successful email sending
			}, (error) => {
			console.log('Error sending email:', error);
			// Handle any errors that occur during email sending
			});
	};

	const handleChange = (e) => {
		setFormData({ ...formData, [e.target.name]: e.target.value });
	};

	return (
		<div className="w-full">
			<motion.div
			ref={ref}
			initial={{ opacity: 0, y: 180 }}
			animate={ controls }
			transition={{
				ease: 'easeInOut',
				duration: 0.7,
				delay: 0.2,
			}}
			className="leading-loose">
			<form className="max-w-xl mx-auto p-6 bg-secondary-light dark:bg-secondary-dark rounded-xl shadow-xl text-left" onSubmit={sendEmail}>
				<p>
					<h2 className="text-2xl font-medium text-primary-dark dark:text-primary-light mb-8">Contact Form</h2>
				</p>
				{isSuccess && (
					<div className="bg-green-200 text-green-700 py-2 px-4 rounded mb-4">
						Message sent successfully!
					</div>
				)}
				<div className="mb-4">
					<label className="block mb-2 text-lg text-primary-dark dark:text-primary-light" htmlFor="name">Your Name</label>
					<input
					className="w-full px-4 py-2 border border-gray-300 dark:border-primary-dark border-opacity-50 text-primary-dark dark:text-secondary-light bg-ternary-light dark:bg-ternary-dark rounded-md shadow-sm"
					type="text"
					id="name"
					name="name"
					placeholder="Your Name"
					value={formData.name}
					onChange={handleChange}
					/>
				</div>
				<div className="mb-4">
					<label className="block mb-2 text-lg text-primary-dark dark:text-primary-light" htmlFor="email">Your Email</label>
					<input
					className="w-full px-4 py-2 border border-gray-300 dark:border-primary-dark border-opacity-50 text-primary-dark dark:text-secondary-light bg-ternary-light dark:bg-ternary-dark rounded-md shadow-sm"
					type="email"
					id="email"
					name="email"
					placeholder="Your Email"
					value={formData.email}
					onChange={handleChange}
					/>
				</div>
				<div className="mb-4">
					<label className="block mb-2 text-lg text-primary-dark dark:text-primary-light" htmlFor="message">Your Message</label>
					<textarea
					className="w-full px-4 py-2 border border-gray-300 dark:border-primary-dark border-opacity-50 text-primary-dark dark:text-secondary-light bg-ternary-light dark:bg-ternary-dark rounded-md shadow-sm"
					id="message"
					name="message"
					rows="6"
					placeholder="Your Message"
					value={formData.message}
					onChange={handleChange}
					></textarea>
				</div>
				<button className="w-full px-4 py-2 mt-4 text-lg text-white bg-indigo-500 hover:bg-indigo-600 rounded-lg" type="submit">Send</button>
				</form>
			</motion.div>
		</div>
	);
};

export default ContactForm;
