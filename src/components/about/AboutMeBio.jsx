import { motion, useAnimation } from 'framer-motion';
import profileImage from '../../images/profile.JPG';
import { useContext, useEffect } from 'react';
import AboutMeContext from '../../context/AboutMeContext';
import { useInView } from 'react-intersection-observer';

const AboutMeBio = () => {
  const { aboutMe } = useContext(AboutMeContext);
  const controls = useAnimation();
  const [ref, inView] = useInView({
    triggerOnce: true,
    rootMargin: '-100px 0px', // Adjust rootMargin as needed
  });

  useEffect(() => {
    if (inView) {
      controls.start({
        opacity: 1,
        x: 0,
        transition: { ease: 'easeInOut', duration: 0.7, delay: 0.1 },
      });
    }
  }, [controls, inView]);

	return (
		<div className="flex p-10">
			<motion.div ref={ref} initial={{ opacity: 0, x: -180 }} animate={controls} transition={{ ease: 'easeInOut', duration: 0.7, delay: 0.2 }} className="mb-7 sm:mb-0">
				<img src={profileImage} className="rounded-2xl h-auto shadow-2xl w-full hidden lg:block contrast-150" style={{ maxHeight: "500px", maxWidth: "500px", overflow: 'hidden'}} alt="" />
			</motion.div>
			<div className="font-general-regular sm:w-3/4 text-left">
				{aboutMe.map((bio) => (
					<p
						className="mt-2 mb-4 pl-10 text-white text-lg"
						key={bio.id}
					>
						{bio.bio}
					</p>
				))}
			</div>
		</div>
	);
};

export default AboutMeBio;
