import React, { useState } from 'react';
import { FaReact, FaBootstrap } from 'react-icons/fa';
import { BiLogoMongodb, BiLogoPostgresql, BiLogoDjango, BiLogoDocker, BiLogoJavascript, BiLogoPython } from 'react-icons/bi';
import { IoLogoCss3 } from 'react-icons/io';
import { AiFillHtml5, AiFillGithub } from 'react-icons/ai';
import { SiRedux, SiTailwindcss, SiFastapi, SiRabbitmq } from 'react-icons/si';
import { motion, AnimatePresence } from 'framer-motion';

const AboutMeSkills = () => {

  const [activeCategory, setActiveCategory] = useState('front-end')

  const fronEndSkills = [
    { icon: <AiFillHtml5 size={32} color="orange" />, title: 'Hypertext Markup Language', description: 'HTML' },
    { icon: <IoLogoCss3 size={32} color="blue" />, title: 'Cascading Style Sheets', description: 'CSS' },
    { icon: <FaReact size={32} color="cyan" />, title: 'Javascript Library', description: 'React' },
    { icon: <SiRedux size={32} color="violet" />, title: 'State Management', description: 'Redux' },
    { icon: <FaBootstrap size={32} color="purple" />, title: 'Front-End Framework', description: 'Bootstrap' },
    { icon: <SiTailwindcss size={32} color="teal" />, title: 'CSS Framework', description: 'Tailwind' },
  ]

  const backEndSkills = [
    { icon: <BiLogoMongodb size={32} color="green" />, title: 'Document-oriented NoSQL Database', description: 'MongoDB' },
    { icon: <BiLogoPostgresql size={32} color={"#4682B4"} />, title: 'Relational Database Management System', description: 'PostgreSQL' },
    { icon: <SiFastapi size={32} color="teal" />, title: 'Web API Framework for Python', description: 'FastAPI' },
    { icon: <BiLogoDjango size={32} color={"#00FF7F"} />, title: 'Web Application Framework for Python', description: 'Django' },
  ]

  const versatileSkills= [
    { icon: <AiFillGithub size={32} color="white" />, title: 'Version Control System', description: 'Git' },
    { icon: <SiRabbitmq size={32} color="orange" />, title: 'Message Broker Middleware', description: 'RabbitMQ' },
    { icon: <BiLogoDocker size={32} color="blue" />, title: 'Container Platform', description: 'Docker' },
    { icon: <BiLogoJavascript size={32} color="yellow" />, title: 'General-Purpose Programming Language', description: 'Javascript' },
    { icon: <BiLogoPython size={32} color="white" />, title: 'Programming Language for Web Development', description: 'Python' },
  ]

  const handleCategoryChange = (category) => {
    setActiveCategory(category)
  }

  const cardsToShow = activeCategory === 'front-end' ? fronEndSkills : activeCategory === 'back-end' ? backEndSkills : versatileSkills

  return (
    <>
    <div className="border-2 border-black bg-[#a6cfd5] p-4 mt-16 mb-16 mx-auto">
      <section className="w-full items-center mx-auto">
        <div className="flex flex-col w-full font-raleway">
          <h1 className="mx-auto text-3xl p-8 font-bold">What I know</h1>
          <div className="flex justify-center items-center w-full mb-8 space-x-4">
            <button className='border py-3 px-6 rounded-lg text-gray-400 hover:text-black hover:border-black bg-black text-white hover:bg-transparent' onClick={() => handleCategoryChange('front-end')}>
              Front End
            </button>
            <button className='border py-3 px-6 rounded-lg text-gray-400 hover:text-black hover:border-black bg-black text-white hover:bg-transparent' onClick={() => handleCategoryChange('back-end')}>
              Back End
            </button>
            <button className='border py-3 px-6 rounded-lg text-gray-400 hover:text-black hover:border-black bg-black text-white hover:bg-transparent' onClick={() => handleCategoryChange('versatile-skills')}>
              Versatile Skills
            </button>
          </div>
          <div className="flex justify-center items-center w-full mb-4">
            <div className="grid grid-cols-2 md:grid-cols-3 gap-4 sm:gap-6 md:gap-10 lg:w-[850px]">
            <AnimatePresence>
              {cardsToShow.map((card) => (
                <motion.div
                  key={card.title}
                  initial={{ opacity: 0 }}
                  animate={{ opacity: 1 }}
                  // exit={{ opacity: 0, y: -20 }}
                  transition={{ ease: 'easeInOut', duration: 0.7, delay: 0.2 }}
                  className="relative sm:flex flex-shrink-0 md:block flex-row items-center rounded-xl md:rounded-2xl p-6 space-x-0 sm:space-x-6 md:space-x-0 w-full h-full md:h-[250px] md:w-[250px] hover:bg-white bg-black text-white hover:border-black border-2 hover:text-black cursor-pointer transition-all duration-300 ease-in-out">
                    {card.icon}
                  <div className="md:absolute bottom-6 px-6 left-0 ">{card.title}</div>
                  <p className="text-base font-normal sm:text-xl md:text-lg xl:text-xl font-raleway sm:font-medium pr-2 mt-2 md:mb-2">{card.description}</p>
                </motion.div>
              ))}
              </AnimatePresence>
            </div>
          </div>
        </div >
      </section>
    </div>
    </>
  );
};

export default AboutMeSkills;
