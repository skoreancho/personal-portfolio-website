import { motion } from 'framer-motion';
// import ContactDetails from '../components/contact/ContactDetails';
import ContactForm from '../components/contact/ContactForm';

const Contact = () => {
	return (
		<motion.div
			initial={{ opacity: 0 }}
			animate={{ opacity: 1 }}
			transition={{
				ease: 'easeInOut',
				duration: 0.5,
				delay: 0.1,
			}}
			className="container mx-auto grid grid-cols-1 lg:flex-row py-5 lg:py-10"
		>
			<div className='h1 mt-10 text-ternary-dark text-center mb-4 dark:text-primary-light'>Contact Me</div>
			<div className="h2 text-center mb-4 text-ternary-dark dark:text-primary-light">Reach out to me with a message!</div>
			<ContactForm />
		</motion.div>
	);
};

export default Contact;
