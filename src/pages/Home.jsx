import AppBanner from '../components/shared/AppBanner';
import ProjectsGrid from '../components/projects/ProjectsGrid';
import Contact from './/Contact';
import About from './/AboutMe';

const Home = () => {
	return (
		<>
		<div className="min-h-screen">
			<AppBanner></AppBanner>
		</div>
		<div id="about" className="items-center bg-[#26408b]">
			<About></About>
		</div>
		<div id="projects" className="h-screen container mx-auto">
			<ProjectsGrid></ProjectsGrid>
		</div>
		<div id="contact" className="min-h-screen w-full bg-[#91e5f6] dark:bg-[#133c55]">
			<Contact></Contact>
		</div>
		</>
	);
};

export default Home;
