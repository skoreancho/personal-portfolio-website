import AboutMeBio from '../components/about/AboutMeBio';
import { AboutMeProvider } from '../context/AboutMeContext';
import { motion } from 'framer-motion';
import AboutMeSkills from '../components/about/AboutMeSkills'

const About = () => {
	return (
		// <div className="items-center container mx-auto">
		<>
		<div className="h1 text-white mt-20 text-center text-ternary-dark dark:text-primary-light">Who I am</div>
		 <AboutMeProvider>
					<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, delay: 1 }}
				exit={{ opacity: 0 }}
				className="container mx-auto"
			>
				<AboutMeBio />

			</motion.div>
			<motion.div
				initial={{ opacity: 0 }}
				animate={{ opacity: 1, delay: 1 }}
				exit={{ opacity: 0 }}
				className=""
			>
				<AboutMeSkills />
			</motion.div>
		</AboutMeProvider>
		</>
	);
};

export default About;
