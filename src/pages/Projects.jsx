import ProjectsGrid from '../components/projects/ProjectsGrid';

const Projects = () => {
	return (
		<div>
			<ProjectsGrid/>
		</div>
	);
};

export default Projects;
