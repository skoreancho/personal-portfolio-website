import React, { useEffect, useState } from "react";
import useThemeSwitcher from './hooks/useThemeSwitcher';
import Typewriter from "typewriter-effect";

export default function Typer() {
  const [activeTheme] = useThemeSwitcher();
  const [textColor, setTextColor] = useState(activeTheme === 'dark' ? "#576CBC" : "#A5D7E8");

  // useEffect(() => {
  //   // Update the text color whenever the theme changes
  //   setTextColor(activeTheme === 'dark' ? "#576CBC" : "#A5D7E8");
  // }, [activeTheme]);

  return (
    <div>
      <Typewriter
        options={{
          strings: ['Full Stack Developer', 'React Web Developer', 'Python Enthusiast'],
          autoStart: true,
          loop: true,
          wrapperClassName: `text-5xl font-bold`, // Use the dynamic textColor variable here
          deleteSpeed: 90,
          delay: 60,
        }}
      />
    </div>
  );
}
